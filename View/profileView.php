<?php $title="Profil"; ?>
<?php ob_start(); ?>
    <div class="wrapaside" id="Profile">
        <section>
            <h1>
                Profil
            </h1>
            <form method="post" action="index.php?action=editUser" >
                <label>Email</label> <input type="email" name="newEmail" value=<?= $user[0]['Email'] ?> >
                <label>Prénom</label>  <input type="text" name="newFirstname" value=<?= $user[0]['Prénom'] ?> >
                <label>Nom</label>  <input type="text" name="newName" value=<?= $user[0]['Nom'] ?> >
                <input type="submit" value="Éditer" >
            </form>
        </section>
        <section>
            <h1>
                Changer de mot de passe
            </h1>
            <form method="post" action="index.php?action=changeMdp" >
                <label>ancien mdp</label> <input type="password" name="oldPass" />
                <label>nouveau mdp</label>  <input type="password" name="newPass" />
                <label>Confirmation</label>  <input type="password" name="newPassConfirmation" />
                <input type="hidden" name="email" value="<?= $_SESSION['email'] ?>" />
                <input type="submit" value="Changer" />
            </form>
            <form method="post" action="index.php?action=DELETE" >
                <input type="submit" value="supprimer votre compte" />
            </form>
        </section>
    </div>
<?php $content = ob_get_clean(); ?>