<?php $title=$_GET['card']; ?>
    <h1>
        Commentaires
    </h1>
    <?php if(isset($_SESSION['email'])) { ?>
        <h2>
            Ajouter un commentaire
        </h2>
        <form method="post" action="index.php?action=postComment" class="commentEdit/Add" >
            <input type="hidden" name="email" value="<?= $_SESSION['email'] ?>" />
            <input type="hidden" name="card" value="<?= $_GET['card'] ?>" />
            <textarea name="com" cols="75" rows="5"></textarea>
            <input type="submit" value="ajouter" />
        </form>
    <?php }else{ ?>
        <p>
            Vous devez être connecté pour poster des commentaires
        </p>
    <?php } ?>
    <?php foreach ($comments as $comment): ?>
        <div id="commentaires">
            <div >
                <p>
                    auteur :
                </p>
                <p>
                    <?= $comment ['Email'] ?>
                </p>
                <p>
            </div>
            </p>
            <div>
                <p>
                    date :
                </p>
                <p>
                    <?= $comment ['date'] ?>
                </p>
                <p>
            </div>
            <p>
                <?= $comment['content'] ?>
            </p>
            <div class="sameLine">

                <?php
                if(isset($_SESSION['email'])) {
                    if ($_SESSION['email'] == $comment['Email']) {
                ?>
                    <form method="post" action="index.php?action=toEdit">
                        <input type="hidden" name="card" value="<?= $_GET['card'] ?>"/>
                        <input type="hidden" name="id" value="<?= $comment['ID'] ?>"/>
                        <input type="submit" value="éditer"/>
                    </form>
                    <?php
                    }if(($_SESSION['email'] == $comment['Email']) or $_SESSION['isAdmin']==1){
                        echo "<form method=\"post\" action=\"index.php?action=delComment\" />";
                        echo '<input type="hidden" name="card" value="'.$_GET['card'].'" />';
                        echo '<input type="hidden" name="id" value="'.$comment['ID'].'" />';
                        echo '<input type="submit" value="supprimer" />';
                        echo "</form>";
                    }
                }
                ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<?php $content = ob_get_clean(); ?>