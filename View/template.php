
<html>
	<head>
		<meta charset="utf-8" />
        <link rel="stylesheet" href="CSS/style.css" />
		<title><?= $title ?></title>
	</head>
	<body>
		<header>
		<div id="navbar">
            <a href="index.php">Acceuil |</a>
            <?php
            if(isset($_SESSION['isAdmin']) and $_SESSION['isAdmin']==1){
                echo '<a href="index.php?action=addCard">Ajouter une carte|</a>';
                echo '<a href="index.php?action=admin">Administration|</a>';
            }
            if(isset($_SESSION['email'])){
                echo '<a href="index.php?action=profile">Profil |</a>';
                echo '<a href="index.php?action=logout"> Déconnexion  </a>';
            }
            else{
                echo '<a href="index.php?action=signup">Inscription  |</a>';
                echo '<a href="index.php?action=signin"> Connexion</a>';
            }
            ?>

		</div>
        <a href="index.php">
            <div  id="Logo">
                <img src="img/Logo/YuGiOH.png" alt="logo" />
                <p>DATABASE</p>
            </div>
        </a>
		</header>
        <div id="Contenue" >
            <?= $content ?>
        </div>
	</body>
</html>
