
<?php ob_start(); ?>
<div class="infoBox">
    <?php
    if(isset($_SESSION['isAdmin']) and $_SESSION['isAdmin']==1){
    ?>
        <div class="Sameline">
            <form method="post" action="index.php?action=delCard">
                <input type="hidden" name="card" value="<?= $_GET['card'] ?>" />
                <input type="submit" value="Supprimer" />
            </form>
            <form method="post" action="index.php?action=toEditCard">
                <input type="hidden" name="card" value="<?= $_GET['card'] ?>" />
                <input type="submit" value="Éditer" />
            </form>
        </div>
    <?php
    }
    ?>
    <h1>
        <?= $cardinfo[0]['cardName'] ?>
    </h1>
    <div class="info">
        <p>
            Type de carte :
        </p>
        <p>
            <?= $cardinfo[0]['cardType'] ?>
        </p>
    </div>
    <div class="info">
        <p>
            Type :
        </p>
        <p>
            <?= $cardinfo[0]['Type'] ?>
        </p>
    </div>
    <div class="info">
        <p>
            niveau :
        </p>
        <p>
            <?= $cardinfo[0]['level'] ?>
        </p>
    </div>
    <div class="info">
        <p>
            Attribut :
        </p>
        <p>
            <?= $cardinfo[0]['cardAttribute'] ?>
        </p>
    </div>
    <div class="info">
        <p>
            Attaque :
        </p>
        <p>
            <?= $cardinfo[0]['Atk'] ?>
        </p>
    </div>
    <div class="info">
        <p>
            Défense :
        </p>
        <p>
            <?= $cardinfo[0]['Def'] ?>
        </p>
    </div>
    <div class="effet">
        <p>
            Effet :
        </p>
        <p>
             <?= $cardinfo[0]['Effet'] ?>
        </p>
    </div>
