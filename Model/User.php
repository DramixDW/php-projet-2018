<?php
require "/Model/Model.php";

class User extends Model
{
    public function addUser(){
        $sql = "insert into utilisateurs (Email, Nom, Prénom, password)"
            ." values (:email, :nom , :prenom, :mdp)" ;
        $params["email"]=$_POST["email"];
        $params["nom"]=$_POST["name"];
        $params["prenom"]=$_POST["firstname"];
        $params["mdp"]=hash("sha512",$_POST["password"]);
        $this->Request($sql,$params);
    }

    public function getPass(){
        $sql = 'SELECT password,isAdmin FROM utilisateurs'
              .' WHERE Email = :email ' ;
        $params["email"]=$_POST["email"];
        $user = $this->Request($sql,$params);
        return $user->fetchAll();
    }

    public function getUsers(){
        $sql="SELECT * from utilisateurs";
        $users = $this->Request($sql);
        return $users->fetchAll();
    }

    public function getUser(){
        $sql = 'SELECT * FROM utilisateurs'
            .' WHERE Email = :email ' ;
        if(!isset($_POST['email'])) {
            $params["email"] = $_SESSION["email"];
        }
        else{
            $params["email"] = $_POST['email'];
        }
        $user = $this->Request($sql,$params);
        return $user->fetchAll();

    }

    public function updateUser(){
        $sql = "UPDATE utilisateurs SET Email = :newEmail, Nom = :newName,"
        ."Prénom = :newFirstname "
        ."WHERE utilisateurs.Email = :oldEmail ";
        $params["newEmail"] = $_POST['newEmail'];
        $params["newName"] = $_POST['newName'];
        $params["newFirstname"] = $_POST['newFirstname'];
        if(!isset($_POST['toEdit'])) {
            $params["oldEmail"] = $_SESSION['email'];
        }
        else{
            $params["oldEmail"] = $_POST['toEdit'];
        }
        $this->Request($sql,$params);
    }

    public function updatePassword(){
        $sql = "UPDATE utilisateurs SET password = :newPass"
            ." WHERE utilisateurs.Email = :email ";
        $params["newPass"] = hash("sha512",$_POST["newPass"]);
        $params["email"] = $_POST['email'];
        $this->Request($sql,$params);
    }
    public function delPLZ(){
            $sql="DELETE from utilisateurs"
                ." WHERE utilisateurs.Email = :Email";
            if(isset($_POST['delEmail'])){
                $params['Email']=$_POST['delEmail'];
            }
            else {
                $params['Email'] =$_SESSION['email'];
            }
            $this->Request($sql,$params);
    }
}