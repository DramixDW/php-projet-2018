<?php
/**
 * Created by PhpStorm.
 * User: romain.verliefden
 * Date: 30-05-18
 * Time: 16:51
 */

class Comment extends Model
{

    function getComments(){
        $sql = 'select * from comments'
            . ' WHERE cardName = :cardName';
        $param['cardName']=$_GET['card'];
        $cards = $this->Request($sql,$param);
        return $cards->fetchAll();
    }

    function addComment(){
        $sql = "insert into comments (Email, cardName, content, date)"
            ." values (:email, :cardName , :content, :date)" ;


        $params["email"]=$_POST["email"];
        $params["cardName"]=$_POST["card"];
        $params["content"]=$_POST["com"];
        $params["date"]= date('Y-m-d H:i:s');
        $this->Request($sql,$params);
    }

    function deleteComment(){
        $sql="DELETE from comments"
            ." WHERE comments.ID = :id";
        $params['id']=$_POST['id'];
        $this->Request($sql,$params);
    }

    function editComment(){
        $sql = "UPDATE comments SET content = :comment"
            ." WHERE comments.id = :id ";
        $params["comment"] = $_POST['com'];
        $params["id"] = $_POST['id'];
        $this->Request($sql,$params);
    }

}