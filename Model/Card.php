<?php
require_once "Model.php";
class Card extends Model {

    public function getCards() {
        $sql = 'select cardName as name, cardType,'
            . ' level as lvl, type  from cards' ;
        $cards = $this->Request($sql);
        return $cards;
    }

    public function ResearchCards(){
        $sql = 'select cardName as name, cardType,'
            . ' level as lvl, type  from cards WHERE cardName LIKE :nameLike'
            . ' AND cardType LIKE :cardTypeLike AND level LIKE :level AND type LIKE :typeLike';

            $params["nameLike"]="%".$_POST["cardName"]."%";
            $params["cardTypeLike"]="%".$_POST["cardType"]."%";
            $params["level"]="%".$_POST["cardLevel"]."%";
            $params["typeLike"]="%".$_POST["type"]."%";
        $cards = $this->Request($sql,$params);
        return $cards->fetchAll();
    }

    public function cardInfo(){
        $sql = 'select * from cards'
            .  ' WHERE cardName = :name ' ;
        if(isset($_GET['card'])) {
            $params['name'] = $_GET['card'];
        }
        else{
            $params['name'] = $_POST['card'];
        }
        $card = $this->Request($sql,$params);
        return $card->fetchAll();
    }

    public function delcard(){
            $sql="DELETE from cards"
                ." WHERE cards.cardName = :name";
            $params['name']=$_POST['card'];
            $this->Request($sql,$params);
    }

    public function updateCard(){
        if($_POST['cardType']=="Spell" or $_POST['cardType']=="Trap"){
            $sql = "UPDATE cards SET cardName = :newCardName,"
                . "cardType = :newCardType, type = :newType, Effet = :newEffet"
                . " WHERE cards.cardName = :oldCardName ";
            $params["newCardName"] = $_POST['card'];
            $params["newCardType"] = $_POST['cardType'];
            $params["oldCardName"] = $_POST['oldCard'];
            $params["newType"] = $_POST['type'];
            $params["newEffet"] = $_POST['Effet'];
            $this->Request($sql, $params);
        }
        elseif ($_POST['cardType']=="Monster") {
            $sql = "UPDATE cards SET cardName = :newCardName, level = :newLevel,"
                . "cardType = :newCardType, type = :newType, Effet = :newEffet, Atk = :newAtk, Def = :newDef, cardAttribute= :newAttribute"
                . " WHERE cards.cardName = :oldCardName ";
            $params["newCardName"] = $_POST['card'];
            $params["newLevel"] = $_POST['level'];
            $params["newCardType"] = $_POST['cardType'];
            $params["oldCardName"] = $_POST['oldCard'];
            $params["newType"] = $_POST['type'];
            $params["newEffet"] = $_POST['Effet'];
            $params["newAtk"] =intval($_POST['Atk']);
            $params["newDef"] =intval($_POST['Def']);
            $params["newAttribute"] = $_POST['cardAttribute'];
            $this->Request($sql, $params);
        }
        else{
            throw new Exception("Type de carte inccorect");
        }
    }

    public function addCard(){
        if($_POST['cardType']=="Spell" or $_POST['cardType']=="Trap"){
            $sql = "insert into cards (cardName, cardType, type, Effet)"
                ." values (:CardName, :CardType , :Type, :Effet)" ;
            $params["CardName"] = $_POST['card'];
            $params["CardType"] = $_POST['cardType'];
            $params["Type"] = $_POST['type'];
            $params["Effet"] = $_POST['Effet'];
            $this->Request($sql, $params);
        }
        elseif ($_POST['cardType']=="Monster") {
            $sql = "insert into cards (cardName, cardType, type, Effet, Atk, Def, cardAttribute,level)"
                ." values (:CardName, :CardType , :Type, :Effet, :Atk, :Def, :Attribute, :Level)" ;
            $params["CardName"] = $_POST['card'];
            $params["Level"] = $_POST['level'];
            $params["CardType"] = $_POST['cardType'];
            $params["Type"] = $_POST['type'];
            $params["Effet"] = $_POST['Effet'];
            $params["Atk"] = $_POST['Atk'];
            $params["Def"] = $_POST['Def'];
            $params["Attribute"] = $_POST['cardAttribute'];
            $this->Request($sql, $params);
        }
        else{
                throw new Exception("Type de carte inccorect");
            }
        }

}