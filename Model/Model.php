<?php

abstract class Model{

	private function getBdd() {
    	$bdd = new PDO('mysql:host=localhost;dbname=yugiohdatabase;charset=utf8', 'root',
      	'', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    	return $bdd;
  	}

  	protected function Request($sql, $params = null) {
	    if ($params == null) {
	      $result = $this->getBdd()->query($sql);
	    }
	    else {
	      $result = $this->getBdd()->prepare($sql);
	      $result->execute($params);
	    }
	    return $result;
	}
		
}

