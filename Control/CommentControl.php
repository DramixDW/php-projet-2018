<?php
require_once "/Model/Comment.php";

function cardComment(){
    $commentControl = new Comment();
    $comments=$commentControl->getComments();
    require_once "/View/commentView.php";
    require_once "/View/template.php";
}

function newComment()
{
    echo $_POST["card"];
    $commentControl = new Comment();
    $commentControl->addComment();
    header("location:index.php?action=carte&card=" . $_POST['card']);
    exit();
}

function delCom(){
    $commentControl = new Comment();
    $commentControl->deleteComment();
    header("location:index.php?action=carte&card=" . $_POST['card']);
    exit();
}

function editView(){
    require_once "/View/editCommentView.php";
    require_once "/View/template.php";
}

function editComComplete(){
    $commentControl = new Comment();
    $commentControl->editComment();
    header("location:index.php?action=carte&card=" . $_POST['card']);
    exit();
}