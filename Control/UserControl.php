<?php
    require_once "Model/User.php";

    function signIn(){
        require_once "/View/signInView.php";
        require_once "/view/template.php";
    }
    function signUp(){
        require_once "/View/signUpView.php";
        require_once  "/View/template.php";
    }

    function completeSignup(){
        formIsCorrect();
        $userControl = new User();
        $userControl->addUser();
        header('Location: index.php');
        exit;
    }

    function signInComplete(){
        formIsCorrect();
        $userControl = new User();
        $user=$userControl->getPass();
        var_dump($user);
        if(!empty($user)){
            if(hash("sha512",$_POST["password"]) == $user[0]['password']){
                $_SESSION['email'] = $_POST["email"];
                $_SESSION['isAdmin'] = $user[0]['isAdmin'];
                header("location: index.php");
                exit();
            }
            else{
                throw new Exception("Mauvais mot de passe");
            }
        }
        else{
            throw new Exception("L'email n'est pas enregistré");
        }

    }

    function logout(){
        session_destroy();
        header("location: index.php");
        exit();
    }

    function profileView(){
        $userControl = new User();
        $user=$userControl->getUser();
        require_once "/View/profileView.php";
        require_once "View/template.php";
    }

    function editProfile(){
        $userControl = new User();
        if(strlen($_POST['newName']) <= 30 and strlen($_POST['newFirstname']) <= 20) {
            $userControl->updateUser();
            if(isset($_SESSION['isAdmin']) and $_SESSION['isAdmin']==1){
                header("location:index.php?action=admin");
            }else {
                $_SESSION['email'] = $_POST['newEmail'];
                header("location:index.php");
            }
            exit();
        }else{
            throw new Exception("Nom ou prénom trop long");
        }
    }
    

    function editPassword(){
        $userControl = new User();
        $user=$userControl->getPass();
        if(hash("sha512",$_POST["oldPass"])==$user[0]['password']){
            if($_POST['newPass']==$_POST['newPassConfirmation']){
                $userControl->updatePassword();
                header("location:index.php");
                exit();
            }
            else{
                throw new Exception("le nouveau mot de passe ne correspond pas à la confirmation");
            }
        }
        else{
            throw new Exception("Mot de passe incorrect");
        }

    }

    function formIsCorrect(){
        if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) !== false and strlen($_POST['email']) <= 75) {
            if(strlen($_POST['password']) >= 4) {
                if(isset($_POST['name'])){
                    if(strlen($_POST['name'])<=30) {
                        if(isset($_POST['firstname'])) {
                            if (strlen($_POST['firstname']) <= 20) {
                                return true;
                            }
                            else{
                                throw new Exception("Prénom trop long");
                            }
                        }
                    }
                    else{
                        throw new Exception("Nom trop long");
                    }
                }
            }
            else {
                throw new Exception("Mot de passe trop court");
            }
        }
        else{
            throw new Exception("Email non valide");
        }

    }

    function userDel(){
        $userControl= new User();
        $userControl->delPLZ();
        if(!isset($_POST['delEmail'])) {
            header("location:index.php");
            session_destroy();
        }
        else{
            header("location:index.php?action=admin");
        }
        exit();
    }

    function userAdmin(){
        if(isset($_SESSION['isAdmin']) and $_SESSION['isAdmin']==1){
            $userControl = new User();
            $users=$userControl->getUsers();
            require_once "/View/userAdminView.php";
            require_once "/View/template.php";
        }
        else{
            throw new Exception("Vous n'êtes pas autorisé à être ici");
        }
    }

    function adminEditUser(){
        if(isset($_SESSION['isAdmin']) and $_SESSION['isAdmin']==1){
            $userControl= new User();
            $user=$userControl->getUser();
            require_once "/View/adminEditUserView.php";
            require_once "/View/template.php";
        }
        else{
            throw new Exception("Vous n'êtes pas autorisé à être ici");
        }

    }