<?php
require_once "/Model/Card.php";

function ListCards(){
    $card=new Card();
    $cardList = $card->getCards();
    require_once "/View/cardList.php";
    require_once "/View/template.php";
}

function ListSearchedCards(){
    $card=new Card();
    $cardList = $card->ResearchCards();
    require_once "/View/cardList.php";
    require_once "/View/template.php";
}

function cardinfo(){
    $card=new Card();
    $cardinfo = $card->cardInfo();
    if(empty($cardinfo)) {
        throw new Exception("La carte n'existe pas");
    }
    require_once "/View/cardView.php";
}

function cardDelete(){
    $card= new Card();
    $card->delcard();
    header("location:index.php?action=carte&card=".$_POST['card']);
    exit();
}

function cardEditForm(){
    $card = new Card();
    $cardinfo = $card->cardInfo();
    require_once "/View/editCardView.php";
    require_once "/View/template.php";
}

function cardEdit(){
    $card = new Card();
    if($_POST['oldCard']!=$_POST['card']) {
        cardFormIsCorrect();
        $card = new Card();
        $card->updateCard();
        header("location:index.php?action=carte&card=" . $_POST['card']);
    }
    elseif (!empty($cardinfo=$card->cardInfo())){
        cardFormIsCorrect();
        $card = new Card();
        $card->updateCard();
        header("location:index.php?action=carte&card=" . $_POST['card']);
    }
    exit();
}

function addCardForm(){
    if(isset($_SESSION['isAdmin']) and $_SESSION['isAdmin']==1) {
        require_once "/View/adminAddCardView.php";
        require_once "/View/template.php";
    }
    else{
        throw new Exception("Vous n'êtes pas admin");
    }
}

function addCard(){
    $card = new Card();
    $card->addCard();
    header("location:index.php");
}


function cardFormIsCorrect(){
    if(strlen($_POST['cardType'])<=10){
        if(strlen($_POST['card'])<=40){
            if(strlen($_POST['cardAttribute'])<=5){
                if(strlen($_POST['type'])<=15){
                    if(intval($_POST['level'])<=98 or($_POST['cardType']=="Spell" or $_POST['cardType']=="Trap")){
                        return true;
                    }
                    else{
                        throw new Exception("niveau incorrect");
                    }
                }
                else{
                    throw new Exception("le type est trop long");
                }
            }
            else{
                throw new Exception("L'attribut est trop long");
            }
        }
        else{
            throw new Exception("Le nom de la carte est trop long");
        }
    }
    else{
        throw new Exception("Type de cartes trop long");
    }
}