<?php

    require_once "/Control/UserControl.php";
    require_once "/Control/CardControl.php";
    require_once "/Control/CommentControl.php";
    session_start();

    try {
        if(isset($_GET['action'])) {
            if($_GET['action']=="search")
            {
                ListSearchedCards();
            }
            elseif($_GET['action']=="signup")
            {
                signUp();
            }
            elseif($_GET['action']=="signupComplete")
            {
                completeSignup();
            }
            elseif($_GET['action']=="signin")
            {
                signIn();
            }
            elseif($_GET['action']=="signinComplete")
            {
                signInComplete();
            }
            elseif($_GET['action']=="logout")
            {
                logout();
            }
            elseif($_GET['action']=="profile" and isset($_SESSION['email']))
            {
                profileView();
            }
            elseif($_GET['action']=="editUser")
            {
                editProfile();
            }
            elseif($_GET['action']=="changeMdp")
            {
                editPassword();
            }
            elseif($_GET['action']=="postComment")
            {
                newComment();
            }
            elseif($_GET['action']=="delComment")
            {
                delCom();
            }
            elseif($_GET['action']=="toEdit")
            {
                editView();
            }
            elseif($_GET['action']=="editComComplete")
            {
                editComComplete();
            }
            elseif($_GET['action']=="delCard")
            {
                cardDelete();
            }
            elseif($_GET['action']=="toEditCard" and isset($_POST['card']))
            {
                cardEditForm();
            }
            elseif($_GET['action']=="editCardComplete")
            {
                cardEdit();
            }
            elseif($_GET['action']=="addCard")
            {
                addCardForm();
            }
            elseif($_GET['action']=="addCardComplete")
            {
                addCard();
            }
            elseif($_GET['action']=="DELETE")
            {
                userDel();
            }
            elseif($_GET['action']=="admin")
            {
                userAdmin();
            }
            elseif($_GET['action']=="toEditStupidUser")
            {
                adminEditUser();
            }
            elseif($_GET['action']=="carte" and isset($_GET['card']))
            {
                cardinfo();
                cardComment();
            }
            else{
                ListCards();
            }

        }
        else{
            ListCards();
        }
    } catch(Exception $e) {
        echo $e;
    }
